# How to use branching in Git to add features

The goal of this tutorial is to go over how to add a feature to a project without changing the production code. The reason to do this is to show that the code works as intended without breaking what is already up and running. This will allow for interactions and changes to made to features without interacting with the production code as well. 

How we are going to complete this is by a feature of git called branching. Branching allows a developer to work on a feature without impacting the main code. Once the developer has tested the code and is happy with it the change they can approve the merge request bringing the branched code into the main code. The project owner can use the branch to verify the code and before approving the merge request therefore putting it into the main branch of code. 

For this tutorial we will be adding a form to a blog entry webpage. Currently the webpage only has the blog content and we want to create a form to add comments so we can increase engagement. 

### Prerequisites

Some prerequisites for following along with this project are having git [installed](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) and [configured](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup). You should also be able to move around the file system using the command line or terminal. 

## Basic blog entry
---

Let get started here is the current site in production that we will be working with.

![Web Page](main.png)

There are a few different ways to get the website file to update this project. You can download the code directly from the git repository on gitlab.com or you can use the command line utility.

### Download from git

Go to this URL and download the zip file.

https://gitlab.com/jamestilton1/awesome-blog

Once the file is downloaded the files will need to be extracted to your file system. 

### Use the terminal and git

Use git clone command from the command line to get the project on to your computer. 

`git clone git@gitlab.com:jamestilton1/awesome-blog.git`


### Bring the files up in the terminal
Once you have the files downloaded we can open the terminal if not done already and traverse to the directory where the files were extracted. 

If you used the terminal and git just run this command. 

`cd awesome-blog`

Here we can see all the files that are associated with website. 

## Creating the feature branch
---

Now that we have the code in our local development environment we can create our feature-branch and add the code for the form of our blog website. This will show the power of git in allowing for testing and modification of files without impacting current live files. 

The first step we need to do is create the feature branch. In the awesome-blog directory we can run the command:

`git branch feature-branch`

With this command we now have a new branch available but currently we are still in the main branch. We check what branch we are in by running this command:

`git branch`

This will display what branches we have available to us along with what branch we are on. 

![branch-list](./branches.png)

The main branch has a * next to is showing that this is the current active branch. It also displays all the other branches available. 

Let's change branches to the feature-branch so we can start working on our feature. 

`git checkout feature-branch`

This command will let you know if you have successfully changed branches and if we run a `git branch` command we can see we are now on the feature branch.

![success](success.png)

![on-feature-branches](branches2.png)

if we look at the files in the directory we will see nothing has changed. What we have done is just prepare the environment to allow us to make changes without affecting the main branch. It is like we have put the files in the main branch in stasis. This is one of the main features of version control, this ability to work on files without breaking working files and to allow for rolling back of changes. This also allows for tracking of the changes which is another major feature of version control. 

Now lets add our feature to our blog entry. Again the goal is to add a form to the entry to allow a user to submit a comment. Open the file blog.html with you favorite text editor and paste in this html code between the bottom `<p>` and `<hr>` tags.

    <hr>
    <h3>Add a Comment: </h3>
    <form action="./blog.html">
        <label for="name">Name: </label><br>
        <input type="text" name="name" id="name"><br>
        <label for="message">Message</label><br>
        <textarea name="message" id="Message" cols="30" rows="10"></textarea><br>
        <input type="button" value="Submit">
    </form>

Save the html file and open up the file with your web browser. This will now display the new form that will allow people to add comment. This is just a demo of git so there is a bit more involved with the form to actually do something but this is the basic layout. 

![feature-added](featureAdded.png)

## Staging and Committing Files

Awesome we now have our comment form ready to go to we can stage and commit the files. 

The first step we need to do is stage the changes. What staging does is that is lets git know which files have been updated and are going to be associated with a particular snapshot. 

To stage our file we will go back to command line and use the following command:

`git add blog.html`

What the add command is telling git to do is stage the modifications that we have made to this particular file. We can check out the status of staging by typing:

`git status`

![git-status](gitStatus.png)

This is some super useful information on what is going on with the files and repository. It shows us that we are on the feature-branch branch and it shows what changes are ready to be committed. So now we can move the file step of this project and commit the changes. 

The final step we are going to do is commit the changes to the repository. Committing is the act of saving the state we are at along with making note of what changes we have made since the last commit. When committing the changes we will be asked to provide a message with the commit. This message should be a description of the changes that were made. We will run the following command:

`git commit -m "Added form to blog entry page"`

Great! We have successfully updated our site to have this new feature committed with a message letting others know what changes were made since the last commit. 

### How do we know no changes have happened to main?

Great question! 

So to check this we need to switch back to the main branch. We do this by using:

`git checkout main`

If we open up our blog.html file in a web browser again or refresh the page we can see that all of our changes have disappeared just like we haven't done anything to the file. Which in the case we have not done anything to this file. We created the feature-branch and worked on that file. Going and looking at the raw HTML we can see that there is no change there. All of our changes were made in the feature-branch and do not affect anything in any other branch. 

If we change back to the feature-branch we can see all of our changes again. 

`git checkout feature-branch`

## Next steps and Conclusions

With this feature-branch complete and tested the next step is to work with the dev team to get the changes merged into the main branch so the code can be put into production. 

In this tutorial we have gone over how to create a branch and adding code to the branch without interacting with the code in the main branch. This is the heart of version control allowing changes to be documented and tested before approving and putting into production. 

### Additional Resources
[Git Branching in a Nutshell](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell)